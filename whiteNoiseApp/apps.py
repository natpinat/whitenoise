from django.apps import AppConfig


class WhitenoiseappConfig(AppConfig):
    name = 'whiteNoiseApp'
